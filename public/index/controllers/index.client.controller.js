// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'example' controller
angular.module('index').controller('SearchController', ['$scope', '$routeParams', '$location', 'CarSearch',
	function($scope, $routeParams, $location, CarSearch) {
		// Controller method to save the form information
		//var searchOption = null;
		$scope.searchOptions = [
			{"value": "bmw", "text": "BMW"}, 
			{"value": "mb", "text": "Mercedes-Benz"}, 
			{"value": "porsche", "text": "Porsche"}
		];

		$scope.search = function() {
			// if (this.selectedMake == undefined) {
			// 	console.log("Empty so all!");
			// 	searchOption = new CarSearch({
			// 		make: "all",
			// 		zipcode: this.zipcode
			// 	});
			// } else {
			// 	searchOption = new CarSearch({
			// 		make: this.selectedMake,
			// 		zipcode: this.zipcode
			// 	});
			// }
			var searchOption = new CarSearch({
				carMake: this.selectedMake,
				zipcode: this.zipcode
			});
				console.log("carMake: " + searchOption.carMake);
				console.log("zipcode: " + searchOption.zipcode);
				$location.path(searchOption.carMake); // + '/:' + searchOption.zipcode);
				// console.log("this.selectedMake: " + this.selectedMake);
				// $location.path(this.selectedMake + '/:' + this.zipcode);
		}
}]);
