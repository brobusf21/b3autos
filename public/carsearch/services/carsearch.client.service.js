// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'carsearch' service
angular.module('carsearch').factory('CarSearch', ['$resource', '$q', function($resource, $q) {
	// Use the '$resource' service to return an carsearch '$resource' object
	return $resource('api/cars/:carMake');

	// var search = function() {
	// 	return $resource('api/cars/:zipcode');
	// };

	// var data = $resource('api/cars/:zipcode');

	// var factory = {
	// 	query: function() {
	// 		var deferred = $q.defer();
	// 		deferred.resolve(data);
	// 		//console.log(data);
	// 		return deferred.promise;
	// 	}
	// }
	// return factory;
}]);