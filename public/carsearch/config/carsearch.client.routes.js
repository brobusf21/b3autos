// Invoke 'strict' JavaScript mode
'use strict';

// Configure the 'carsearch' module routes
angular.module('carsearch').config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
		when('/bmw', {
			templateUrl: 'carsearch/views/bmw-index.client.view.html'
		}).
		when('/mb/:zipcode', {
			templateUrl: 'carsearch/views/mb-index.client.view.html'
		}).
		when('/all/:zipcode', {
			templateUrl: 'carsearch/views/all-index.client.view.html'
		});
	}
]); 