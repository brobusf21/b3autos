// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'carsearch' controller
angular.module('carsearch').controller('CarSearchController', ['$scope', '$routeParams', '$location', 'CarSearch',
    function($scope, $routeParams, $location, CarSearch) {
        // Create a new controller method for retrieving a list of cars
        console.log("location!: " + $location.path().substring(1));
        //console.log("routeParams " + $routeParams.zipcode);

        var yearOptions = [];
        var colorOptions = [];
        var modelOptions = [];
        $scope.yearOptions = yearOptions;
        $scope.colorOptions = colorOptions;
        $scope.modelOptions = modelOptions;

        $scope.cars = CarSearch.query({carMake: $location.path()}, function(resource) {
            resource.forEach(function (car) {
                //console.log("car.year: " + car.year);
                //console.log("car.color: " + car.color);
                var yearObj = {"value": car.year};
                var colorObj = {"value": car.color};
                var modelObj = {"value": car.model};
                if (!containsObject(yearObj, yearOptions)) {
                    yearOptions.push({
                        "value": car.year,
                        "text": car.year
                    });
                }
                if (!containsObject(colorObj, colorOptions)) {
                    colorOptions.push({
                        "value": car.color,
                        "text": car.color
                    });
                }
                if (!containsObject(modelObj, modelOptions)) {
                    modelOptions.push({
                        "value": car.model,
                        "text": car.model
                    });
                }
            });
        });

        /* Javascript function to check if an object exists in a list */
        function containsObject(obj, list) {
            console.log("obj.value: " + obj.value);
            for (var i = 0; i < list.length; i++) {
                console.log("list value: " + list[i].value);
                if (list[i].value == obj.value) {
                    return true;
                }
            }
            return false;
        }
    }
]);