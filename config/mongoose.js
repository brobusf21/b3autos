// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var	config = require('./config'),
	mongoose = require('mongoose'),
	uri = 'mongodb://brobusf21:Brobusf2121@ds037185.mongolab.com:37185/b3autos-db';

// Define the Mongoose configuration method
module.exports = function() {
	
	// Use Mongoose to connect to MongoDB
	var db = mongoose.connect(uri);
	//var db = mongoose.connect(config.db);

	// If the connection throws an error
	mongoose.connection.on('error', function (err) {  
	  console.log('Mongoose default connection error: ' + err);
	}); 

	// When the connection is disconnected
	mongoose.connection.on('disconnected', function () {  
	  console.log('Mongoose default connection disconnected'); 
	});

	// Load the 'Car' model 
	require('../app/models/cars.server.model');

	// Load the 'Dealership' model
	require('../app/models/dealership.server.model');

	// Return the Mongoose connection instance
	return db;
};