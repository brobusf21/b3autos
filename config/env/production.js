// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'production' environment configuration object
module.exports = {
	db: 'mongodb://localhost/b3autos',
	sessionSecret: 'productionSessionSecret'
};