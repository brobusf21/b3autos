// Invoke 'strict' JavaScript mode
'use strict';

// Set the 'development' environment configuration object
module.exports = {
	db: 'mongodb://localhost/b3autos',
	sessionSecret: 'developmentSessionSecret'
};