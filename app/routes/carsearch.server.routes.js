// Invoke 'strict' JavaScript mode
'use strict';

// Load the 'cars' controller
var cars = require('../../app/controllers/carsearch.server.controller');
//var dealership = require('../../app/controllers/dealership.server.controller');

// Define the routes module' method
module.exports = function(app) {
	// Set up the 'carsearch' base routes
	// app.route('/api/cars')
	// 	.get(cars.list);
	   	//.get(cars.testingPurposes);
	   	//.get(cars.renderTest);

	app.route('/api/cars/:carMake')
	   .get(cars.listMake);

	// Set up the 'ruleset' parameter middleware   
	app.param('carMake', cars.carsByMake);
};