/* This controller is intended to work with dealership data */

// Invoke 'strict' JavaScript mode
'use strict';

// Load the 'Dealership' Mongoose model
var Dealership = require('mongoose').model('Dealership');

exports.findDealerships = function(req, res, next) {
	var m = req.session.make;
  	Dealership.find({makes: m}, function(err) {
		console.log();
  	});
};