// Invoke 'strict' JavaScript mode
'use strict';

// Load the 'Car' Mongoose model
var Car = require('mongoose').model('Car');
var Dealership = require('mongoose').model('Dealership');
var Q = require('q');
var request = Q.denodeify(require('request'));
var https = require('https');
var Promise = require('bluebird');


/* Method: getCarIndexPage
 * Purpose:  Renders the appropriate cars index page
 * We can now calculate distances and return the correct vehicles!
 */
 exports.getCarIndexPage = function(req, res) {
	var m = req.query.make;
	var zipcode = req.query.userZipcode;
	//var radius = req.session.radius;

	Dealership.find({makes: m}).exec()
		.then(function (ids) {
			var promises = [];
			ids.forEach(function (id) {
			 	/* Pushing ascynchrounous functions into promise array */
			 	promises.push(getDistanceWithQPromise(zipcode, id.zip, id._id));
			});
			return Q.all(promises)
				.then(function (promise) {
					var dealershipIDs = []; /* Ading dealership ids to array */
					promise.forEach(function (promiseData) {
						var distance = promiseData.distance;
						var id = promiseData.id;
						if (distance <= radius) {
							console.log("Adding " + id + " to array");
							dealershipIDs.push(id); // Adding dealership's id to array
							console.log("Size of dealership array: " + dealershipIDs.length);
				 		}
					});
					console.log("Outside for loop = Size of dealership array: " + dealershipIDs.length); /* Does recognize array size */
					return dealershipIDs;
				}, function (err) {
					console.log(err)
				});
		}).then(function (resultArray) { // Receives the dealership Id array
				//console.log("Result array : " + resultArray);
				Car.find({dealership_id: { $in: resultArray }}).exec()
					.then(function (cars) {
						renderResult(res, req, cars);
					}),
					function (error) {
						console.log("Could not iterate through through cars: " + error);
					}	
		}, function (error) {
			console.error("Error with the outer promises:", error);
		});
}


function getDistanceWithQPromise(userInput, dealerZip, dealerID) {
	var deferred = Q.defer();
	var request = https.request('https://www.zipcodeapi.com/rest/xApFwnm4tosuL2gX2UDQIGcknN2NIHyfhXVNlhRPFkjrmzpou2edJry7fAVXhtdz/distance.json/' 
			+ userInput + '/' + dealerZip + '/mile', function(response) {
		var responseData = '';
		response.on('data', function (data) {
			responseData += data;
		});

		response.on('end', function() {
			responseData = responseData.slice(0, -1);
			responseData += "," + '"id":' + '"'+dealerID+'"' + "}";
			console.log(responseData);


			deferred.resolve(JSON.parse(responseData));
		});
				
		});
		request.on('error', function(err) {
				deferred.reject(err);
		});

		request.end();
		return deferred.promise;
};

/* This is used for our CRUD API in which Angular will call this function */
 exports.list = function(req, res) {
	// Use the model 'find' method to get a list of cars

 	Car.find().exec(function(err, cars) {
 		if (err) {
 			return res.status(400).send({
 				message: getErrorMessage(err)
 			});
 		} else {
 			// Send a JSON representation of the cars
 			res.json(cars);
 		}
 	})
 }

 /* This is used for our CRUD API in which Angular will call this function */
 exports.listMake = function(req, res) {
	// Use the model 'find' method to get a list of cars
	console.log("Made it inside of listMake");
	console.log(req.make);
	var m = req.make;
 	Car.find({make: m}).exec(function(err, cars) {
 		if (err) {
 			return res.status(400).send({
 				message: getErrorMessage(err)
 			});
 		} else {
 			// Send a JSON representation of the cars
 			//console.log(cars);
 			res.json(cars);
 		}
 	})
 }

 // Create a new controller method that returns an existing article
exports.read = function(req, res) {
	res.json(req.car);
};

// Create a new controller middleware that retrieves a single existing 
exports.carsByMake = function(req, res, next, make) {
	// Use the model 'findById' method to find cars based on make
		console.log("This is make: " + make.substring(1));
		req.make = make.substring(1);
		// Call the next middleware
		next();
};


/* This function is not used but is saved for reference.
 */
// function getDistanceWithAPI(userInput, dealerZip, callback) {
// 	https.get('https://www.zipcodeapi.com/rest/xApFwnm4tosuL2gX2UDQIGcknN2NIHyfhXVNlhRPFkjrmzpou2edJry7fAVXhtdz/distance.json/' 
// 			+ userInput + '/' + dealerZip + '/mile', function(res) {
// 	  var body = ''; // Will contain the final response

// 	  res.on('data', function(data){
// 	    body += data;
// 	  });

// 	  // After the response is completed, parse it and log it to the console
// 	  res.on('end', function() {
// 	    var parsed = JSON.parse(body);
// 	    //console.log(parsed);
// 	    callback(parsed);
// 	  });
// 	})

// 	// If any error has occured, log error to console
// 	.on('error', function(e) {
// 	  console.log("Got error: " + e.message);
// 	});
// }