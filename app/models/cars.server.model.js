// Invoke 'strict' JavaScript mode
'use strict';

// Load the Mongoose module and Schema object
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// Define a new 'CarSchema'
var CarSchema = new Schema({
	make: {
		type: String
	},
	model: {
		type: String
	},
	year: {
		type: String
	},
	price: { 
		type: String
	},
	color: { 
		type: String
	},
	attributes: {
		type: [String]
	},
	dealershipID: {
		type: String
	}
});

mongoose.model('Car', CarSchema);