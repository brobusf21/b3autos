// Invoke 'strict' JavaScript mode
'use strict';

// Load the Mongoose module and Schema object
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// Define a new 'DealershipSchema'
var DealershipSchema = new Schema({
	_id: String,
	name: String,
	makes: String,
	street: String,
	city: String,
	state: String,
	zip: String
});

// Create the 'Dealership' model out of the 'DealershipSchema'
mongoose.model('Dealership', DealershipSchema);